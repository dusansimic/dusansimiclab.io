---
title: "Hi, I'm Dušan"
---

{{% image src="https://i.ibb.co/hYKh7Sp/20191016-222215.jpg" alt="Dušan" style="width: 250px" %}}

## Social

 * [{{% fa github %}} GitHub](https://github.com/dusansimic)
 * [{{% fa gitlab %}} GitLab](https://gitlab.com/dusansimic)
 * [{{% fa twitter %}} Twitter](https://twitter.com/dusan__simic)
 * [{{% fa instagram %}} Instagram](https://instagram.com/dusan__simic)
 * [{{% fa linkedin %}} LinkedIn](https://www.linkedin.com/in/dusansimic)

## About me

 * Computer Science student (University of Novi Sad)
 * Hacker and maker
 * Open sourcer

## Open source

Since elementary school I've been interested into open source and free software. Throughout highschool, I've worked on multiple personal open source projects (small programs) and all of them are under MIT license. Almost all of my work is under MIT license and you can check it out on my GitHub and GitLab pages.

You can see what I'm working on at the moment on my pinned repos.

## Makers community

I'm an active member of a local maker community. We thrive to offer knowledge without any additional cost. I love to show people new and interesting stuff I'm working on and I can do just that with this community. If you're interested in what we're working on right now, visit [our website](http://maker.rs) or follow us on [Facebook](https://www.facebook.com/MakerNoviSad/) but keep in mind that most of our posts on Facebook page are in Serbian.

## Blog

I've started a blog where I post some things I like to teach others, technologies I'm currently working with and other stuff I'm interested in. Some post might be in Serbian but there are tags to filter posts out. To comment on a specific blog post, please send me an email with the post title and date in subject/body so I can respond to you more easily.
